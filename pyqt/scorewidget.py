from PyQt5 import QtCore, QtGui, QtWidgets, QtMultimedia
from PyQt5.QtWidgets import QPushButton, QLabel, QInputDialog, QProgressBar
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtMultimedia import QSound
from PyQt5.QtGui import QPainter, QBrush, QColor, QPen
from config import VIEW_WIDTH, VIEW_HEIGHT, UNIT_LENGTH, PATH_PREFIX
from beanhintwidget import BeanHintWidget

#积分、提示信息等容器窗口
class ScoreWidget(QtWidgets.QWidget):
    m_total_score = 0 #积分
    m_anger_value = 0 #怒气值

    uploadScore = pyqtSignal(int, str) #上传积分信号
    checkRanking = pyqtSignal()        #查看排行榜信号
    
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.resize(VIEW_WIDTH*UNIT_LENGTH, 4*UNIT_LENGTH)
        self.setStyleSheet("QLabel{color:white;}")

        self.m_audio_1 = QSound(PATH_PREFIX + "static/audio/warning-1.wav")
        self.m_audio_1.setLoops(-1)
        self.m_audio_2 = QSound(PATH_PREFIX + "static/audio/warning-2.wav")
        self.m_audio_2.setLoops(-1)

        self.bean_hint_widget = BeanHintWidget(self)
        self.bean_hint_widget.move((self.width()/2 - self.bean_hint_widget.width())/2,UNIT_LENGTH//2)

        self.m_score_label = QLabel(self)
        self.m_score_label.setGeometry(VIEW_WIDTH*UNIT_LENGTH/2, 2*UNIT_LENGTH, 150, 2*UNIT_LENGTH)
        self.m_score_label.setAlignment(Qt.AlignCenter)
        self.m_score_label.setText("总得分： " + str(self.m_total_score) + " 分")

        self.m_score_upload = QPushButton(self)
        self.m_score_upload.setGeometry(VIEW_WIDTH*UNIT_LENGTH/2+2, UNIT_LENGTH//2, 70, UNIT_LENGTH)
        self.m_score_upload.setText("上传")
        self.m_score_upload.setEnabled(False)
        self.m_score_upload.setToolTip("死亡后，可点击此按钮上传总得分。")
        self.m_score_upload.clicked.connect(self.uploadSlot)

        self.m_rank_btn = QPushButton(self)
        self.m_rank_btn.setGeometry(VIEW_WIDTH*UNIT_LENGTH/2+76, UNIT_LENGTH//2, 70, UNIT_LENGTH)
        self.m_rank_btn.setText("TOP 20")
        self.m_rank_btn.setToolTip("点击榜单按钮，可查看TOP排行榜")
        self.m_rank_btn.clicked.connect(self.rankSlot)

        self.m_anger_label = QLabel(self)
        self.m_anger_label.setGeometry(VIEW_WIDTH*UNIT_LENGTH/2+165, 2*UNIT_LENGTH, 60, 2*UNIT_LENGTH)
        self.m_anger_label.setAlignment(Qt.AlignCenter)
        self.m_anger_label.setText("怒气值： ")
        self.m_p_bar = QProgressBar(self)
        self.m_p_bar.setGeometry(VIEW_WIDTH*UNIT_LENGTH/2+230, 2.5*UNIT_LENGTH, VIEW_WIDTH*UNIT_LENGTH/2-250, UNIT_LENGTH)
        self.m_p_bar.setValue(self.m_anger_value)

        self.m_tricks_label = QLabel(self)
        self.m_tricks_label.setText("可用技能：")
        self.m_tricks_label.setGeometry(VIEW_WIDTH*UNIT_LENGTH/2+160, UNIT_LENGTH//2, 70,20)
        self.m_speed_up_label = QPushButton(self)
        self.m_speed_up_label.setText("加速")
        self.m_speed_up_label.setGeometry(VIEW_WIDTH*UNIT_LENGTH/2+230, UNIT_LENGTH//2, 40,20)
        self.m_speed_up_label.setEnabled(False)
        self.m_speed_up_label.setToolTip("怒气积满后，请按键盘Q键！")
        self.m_speed_down_label = QPushButton(self)
        self.m_speed_down_label.setText("减速")
        self.m_speed_down_label.setGeometry(VIEW_WIDTH*UNIT_LENGTH/2 + 270, UNIT_LENGTH//2, 40,20)
        self.m_speed_down_label.setEnabled(False)
        self.m_speed_down_label.setToolTip("怒气积满后，请按键盘W键！")

        self.m_notify_label = QLabel(self)
        self.m_notify_label.setGeometry(0, 2*UNIT_LENGTH, VIEW_WIDTH*UNIT_LENGTH/2, 2*UNIT_LENGTH)
        self.m_notify_label.setAlignment(Qt.AlignCenter)
        self.m_notify_label.setText("请尽情探索游戏，争取获得更高的积分吧！")

    def paintEvent(self, e): #绘制背景和几个线条
        painter = QPainter(self)
        brush  = QBrush(QColor(121,131,44))
        painter.setBrush(brush)
        painter.drawRect(0,0, self.width(), self.height())

        pen = QPen(Qt.white)
        pen.setWidth(3)
        painter.setPen(pen)
        painter.drawLine(self.width()/2,0, self.width()/2, self.height())
        painter.drawLine(0, 2*UNIT_LENGTH , self.width()/2, UNIT_LENGTH*2)
        painter.drawLine(self.width()/2 + 155, 0, self.width()/2 + 155, self.height())
        painter.drawLine(0, self.height()-2, self.width(), self.height()-2)

    def getTotalScore(self):
        return self.m_total_score

    def resetScore(self):
        self.m_total_score = 0
        self.m_anger_value = 0
        self.displayScore()
        self.m_notify_label.setText("游戏积分已重置，请重新开始！")
        self.m_score_upload.setEnabled(False)

    def resetAnger(self):
        self.m_anger_value = 0
        self.m_speed_up_label.setEnabled(False)
        self.m_speed_down_label.setEnabled(False)
        self.displayScore()

    def getAnger(self):
        return self.m_anger_value


    def addScore(self, score): #蛇吃到Bean后，添加积分方法
        self.m_total_score += score
        self.m_anger_value += score

        if self.m_anger_value >= 100 :
            self.m_anger_value = 100
            self.m_speed_up_label.setEnabled(True)
            self.m_speed_down_label.setEnabled(True)
        else:
            self.m_speed_up_label.setEnabled(False)
            self.m_speed_down_label.setEnabled(False)
       
        self.displayScore()


    def displayScore(self):
        self.m_score_label.setText("总得分： " + str(self.m_total_score) + " 分")
        self.m_p_bar.setValue(self.m_anger_value)

    #切换左上角信息提示
    def switchNotify(self, key = None, status = False, type=-1): #type与豆子的type一致，用来显示效果信息
        if key == None and status == False and type == -1:
            self.playAudio(1,False) #type 1和2都会停止
            self.m_notify_label.setText("技能已失效，请重新积攒怒气值！")
            return

        if key == None:
            self.playAudio(1,status)
            if type < 0: #没有指明任何豆子type
                if status:
                    self.m_notify_label.setText("特效已开启！")
                else:
                    self.m_notify_label.setText("特效已关闭！")
                return
            elif type==2: #黑豆子，反向特效
                if status:
                    self.m_notify_label.setText("反向特效已开启！")
                else:
                    self.m_notify_label.setText("反向特效已关闭！")
                return
            return

        #以上是非技能提示，以下是技能提示

        def q(status):
            self.playAudio(2,status)
            if status:
                self.m_notify_label.setText("已经开启加速技能！")
            else:
                self.m_notify_label.setText("加速技能已失效！")
        
        def w(status):
            self.playAudio(2,status)
            if status:
                self.m_notify_label.setText("已经开启减速技能！")
            else:
                self.m_notify_label.setText("减速技能已失效！")

        switch = {
            Qt.Key_Q: q,
            Qt.Key_W: w, }

        switch.get(key)(status)
    
    #游戏结束时，需要固定切换一种信息提示，并将上传按钮enable
    def gameOver(self, status):
        if status:
            self.m_notify_label.setText("本局游戏已结束，您可以点击分数上传按钮，将您的得分上传至服务器！")
        self.m_score_upload.setEnabled(status)

    #上传按钮点击槽函数，首先要求用户输入姓名，再通过信号传递给主逻辑处理类
    def uploadSlot(self):
        text, ok = QInputDialog.getText(self, "请输入姓名","姓名：")
        if ok and text:
            self.uploadScore[int, str].emit(self.m_total_score, text)

    #排行榜按钮点击槽函数，通过信号传递给主逻辑处理类
    def rankSlot(self):
        self.checkRanking.emit()

    def playAudio(self, type, status): #type表示音频类型，status表示播放还是停止
        if type == 1: #代表吃到了特效豆
            self.m_audio_2.stop()
            if status:
                self.m_audio_1.play()
            else:
                self.m_audio_1.stop()
        elif type == 2: #代表发动了技能
            self.m_audio_1.stop()
            if status:
                self.m_audio_2.play()
            else:
                self.m_audio_2.stop()

        
