

def collisionDetection(target, items): #碰撞检测的方法
    result = []
    for item in items:
        x, y = item.x(), item.y()
        w, h = item.width(), item.height()
        if x <= target.x() and target.x() < x + w and y <= target.y() and target.y() < y+h:
            result.append(item)
    return result
