from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QWidget, QLabel
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt
from config import VIEW_WIDTH, VIEW_HEIGHT, UNIT_LENGTH

#左上角关于Bean种类信息的提示窗口
class BeanHintWidget(QWidget): 
    def __init__(self, parent):
        super().__init__(parent=parent)
        self.setStyleSheet("color:white;")

        self.red_label = QLabel(self)
        self.red_label.setStyleSheet("background-color:rgb(255,0,255); border-radius:%spx" % str(UNIT_LENGTH/2))
        self.red_label.setGeometry(0,0,UNIT_LENGTH,UNIT_LENGTH)
        self.red_name_label = QLabel(self)
        self.red_name_label.setGeometry(UNIT_LENGTH,0,3*UNIT_LENGTH, UNIT_LENGTH)
        self.red_name_label.setText("加长豆")
        self.red_name_label.setAlignment(Qt.AlignCenter)

        self.blue_label = QLabel(self)
        self.blue_label.setStyleSheet(" background-color:rgb(0,0,255); border-radius:%spx" % str(UNIT_LENGTH/2))
        self.blue_label.setGeometry(5*UNIT_LENGTH,0,UNIT_LENGTH,UNIT_LENGTH)
        self.blue_name_label = QLabel(self)
        self.blue_name_label.setGeometry(6*UNIT_LENGTH,0,3*UNIT_LENGTH, UNIT_LENGTH)
        self.blue_name_label.setText("缩短豆")
        self.blue_name_label.setAlignment(Qt.AlignCenter)

        self.black_label = QLabel(self)
        self.black_label.setStyleSheet(" background-color:rgb(0,0,0); border-radius:%spx" % str(UNIT_LENGTH/2))
        self.black_label.setGeometry(10*UNIT_LENGTH,0,UNIT_LENGTH,UNIT_LENGTH)
        self.black_name_label = QLabel(self)
        self.black_name_label.setGeometry(11*UNIT_LENGTH,0,3*UNIT_LENGTH, UNIT_LENGTH)
        self.black_name_label.setText("反向豆")
        self.black_name_label.setAlignment(Qt.AlignCenter)

        self.resize(14*UNIT_LENGTH, UNIT_LENGTH)