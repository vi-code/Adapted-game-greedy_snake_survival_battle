from PyQt5 import QtCore, QtWidgets
from config import DIRECTION_UP, DIRECTION_RIGHT, DIRECTION_NONE, DIRECTION_LEFT, DIRECTION_DOWN
from config import VIEW_HEIGHT, VIEW_WIDTH, UNIT_LENGTH
from PyQt5.QtGui import QPainter, QColor, QBrush, QKeyEvent, QFont
from PyQt5.QtCore import Qt
import sys
import random


def RandomBean(x, y, w, h, e, parent): #随机种类Bean的实现方法
    bt = random.randint(0, 2)
    switch = {
        0: RedBean,
        1: BlueBean, 
        2: BlackBean,}

    return switch.get(bt)(x, y, w, h, e, parent)


class Bean(QtWidgets.QLabel): #Bean基类
    def __init__(self, x, y, w, h, e, parent=None):
        super().__init__(parent=parent)
        self.setGeometry(x, y, w, h)
        self.e = e
        font = QFont()
        font.setPointSize(10)
        self.setFont(font)
        self.setText(str(e))
        self.setAlignment(Qt.AlignCenter)
        self.show()

    def timerOut(self):
        self.e -= 1

    def periodTriggered(self):
        self.e -= 1
        self.setText(str(self.e))

    def getType(self):
        pass

    def getLifeValue(self):
        return self.e


class RedBean(Bean): #身长Bean
    def __init__(self, x, y, w, h, e, parent=None):
        super().__init__(x, y, w, h, e, parent=parent)
        self.setStyleSheet("color:white; background-color:rgb(255,0,255); border-radius:%spx" % str(UNIT_LENGTH/2))

    def getType(self):
        return 'red'


class BlueBean(Bean): #缩短Bean
    def __init__(self, x, y, w, h, e, parent=None):
        super().__init__(x, y, w, h, e, parent=parent)
        self.setStyleSheet("color:white; background-color:rgb(0,0,255); border-radius:%spx" % str(UNIT_LENGTH/2))

    def getType(self):
        return 'blue'

class BlackBean(Bean): #反向Bean
    def __init__(self, x,y,w,h,e, parent=None):
        super().__init__(x, y, w, h, e, parent=parent)
        self.setStyleSheet("color:white; background-color:rgb(0,0,0); border-radius:%spx" % str(UNIT_LENGTH/2))

    def getType(self):
        return 'black'
