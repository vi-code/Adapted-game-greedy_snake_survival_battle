import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QIcon
from repaintwidget import RepaintWidget #游戏主窗口，以重绘为主
from scorewidget import ScoreWidget #得分、信息展示以及功能按钮窗口
from config import PATH_PREFIX

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    main_window = QMainWindow() #主窗口容器
    
    scoreWidget = ScoreWidget(main_window) #得分、信息提示等内容的容器窗口
    scoreWidget.move(0,0)
    
    repaintWidget = RepaintWidget(main_window) #绘制蛇，以及事件主循环的窗口
    repaintWidget.move(0, scoreWidget.height())
    repaintWidget.setScoreWidget(scoreWidget)

    main_window.resize(scoreWidget.width(), scoreWidget.height()+repaintWidget.height())
    main_window.setWindowTitle("贪吃蛇生存大作战")
    main_window.setWindowIcon(QIcon(PATH_PREFIX + "static/pic/logo.ico"))
    main_window.show()

    sys.exit(app.exec_())
