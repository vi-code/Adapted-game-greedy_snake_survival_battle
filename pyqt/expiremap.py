import time
from PyQt5.QtCore import QObject, QTimer

#不定数量对象生命周期的管理类
class ExpireMap(QObject):
    obj_map = {}
    
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        

    def __del__(self):
        pass

    def __iter__(self):
        for i in self.obj_map:
            yield i

    def __getitem__(self, index):
        v = self.obj_map[index]
        return v

    def checkExpire(self):
        t = time.time()
        for k in list(self.obj_map.keys()):
            if self.obj_map[k] < 0:
                continue
            if self.obj_map[k] <= int(t):
                del self.obj_map[k]
                k.deleteLater()

    def Insert(self, obj, expire=-1):
        v = expire
        if expire != -1:
            t = time.time()
            v = int(t) + expire
        self.obj_map[obj] = v

    def Remove(self, key):
        del self.obj_map[key]
        key.deleteLater()

    def items(self):
        return self.obj_map.keys()
