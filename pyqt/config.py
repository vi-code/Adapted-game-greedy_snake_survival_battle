#各个关键参数的设置，可以理解为全局变量或者宏

# 方向宏，有正负值也是为了方便判断和运算
DIRECTION_UP = -2
DIRECTION_RIGHT = -1
DIRECTION_NONE = 0
DIRECTION_LEFT = 1
DIRECTION_DOWN = 2

UNIT_LENGTH = 20 #单位大小，整个游戏界面全部划分为单位大小的网格，蛇的节点就是单位大小
VIEW_WIDTH = 40  # 场景宽，以单位大小为基础
VIEW_HEIGHT = 30 # 场景高，以单位大小为基础


BASE_PERIOD = 1 #基础晶振周期，设置为 1 ms，最小时间粒度；也可调整为其他大小
MOVE_COUNTER = 400 #移动周期计数器， 以基础晶振周期为单元，计数到后蛇移动一次；运行过程中值会被改变
DATUM_MOVE_COUNTER = 400 #基准移动计数器，运行过程中，不会改变
BEAN_LIFE_COUNTER = 1000 #豆子生命计数器，计数到后生命值减1
EXPIRE_COUNTER = 10 #豆子生命周期检测器，管理所有豆子的对象
RANDOM_COUNTER = 1000 #固定时间随机出现豆子的计数器，计数到后开始出现新一轮随机豆

TRICKS_PERIOD = 5000 #技能、特效持续时间和周期

PATH_PREFIX = "./" #代码中有多处使用了资源文件路径，这里利用一个变量方便控制修改
