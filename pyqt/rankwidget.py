from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QWidget, QTableView
from PyQt5.QtCore import Qt,QAbstractTableModel, QModelIndex, QVariant

RANKING,NAME,SCORE = range(3)
HEADERS = ('排名', '姓名', '积分')
CONVERTS_FUNS = (int, None, int)

#ranking 的数据Model，主要为了搭配QTableView使用
class rankModel(QAbstractTableModel):
    def __init__(self, headers=HEADERS):
        super().__init__()
        self.datas = []
        self.headers = headers
        self.load()

    def load(self):
        self.beginResetModel()
        #数据载入
        self.endResetModel

    def loadData(self, rank_data):
        self.datas = rank_data

    def data(self, index, role=Qt.DisplayRole):
        if (not index.isValid or not (0 <= index.row() < len(self.datas))):
            return None
        
        row, col = index.row(),index.column()
        data = self.datas[row]
        if role == Qt.DisplayRole:
            item = data[col]
            if col == RANKING or col == SCORE:
                item = int(item)
            return item
        elif role == Qt.TextAlignmentRole:
            return Qt.AlignCenter
        return None

    def rowCount(self, index=QModelIndex()):
        return len(self.datas)

    def columnCount(self, index=QModelIndex()):
        return len(self.headers)

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return None

        if orientation == Qt.Horizontal:
            return self.headers[section]
        return int(section + 1)